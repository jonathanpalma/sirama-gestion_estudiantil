-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 11-05-2017 a las 12:55:12
-- Versión del servidor: 5.5.55-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sirama`
--
CREATE DATABASE IF NOT EXISTS `sirama` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_spanish2_ci;
USE `sirama`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `city`
--

INSERT INTO `city` (`id`, `name`) VALUES
(1, 'Ahuachapán'),
(2, 'Cabañas'),
(3, 'Chalatenango'),
(4, 'Cuscatlán'),
(5, 'La Libertad'),
(6, 'La Paz'),
(7, 'La Unión'),
(8, 'Morazán'),
(9, 'San Miguel'),
(10, 'San Salvador'),
(11, 'San Vicente'),
(12, 'Santa Ana'),
(13, 'Sonsonate'),
(14, 'Usulután');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `civil_status`
--

CREATE TABLE IF NOT EXISTS `civil_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `civil_status`
--

INSERT INTO `civil_status` (`id`, `name`) VALUES
(1, 'Soltera'),
(2, 'Casada'),
(3, 'Acompañada'),
(4, 'Divorciada'),
(5, 'Viuda');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `sort_name` varchar(3) COLLATE utf8_spanish2_ci NOT NULL,
  `phone_code` varchar(5) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=247 ;

--
-- Volcado de datos para la tabla `country`
--

INSERT INTO `country` (`id`, `name`, `sort_name`, `phone_code`) VALUES
(1, 'Afghanistan', 'AF', '93'),
(2, 'Albania', 'AL', '355'),
(3, 'Algeria', 'DZ', '213'),
(4, 'American Samoa', 'AS', '1684'),
(5, 'Andorra', 'AD', '376'),
(6, 'Angola', 'AO', '244'),
(7, 'Anguilla', 'AI', '1264'),
(8, 'Antarctica', 'AQ', '0'),
(9, 'Antigua And Barbuda', 'AG', '1268'),
(10, 'Argentina', 'AR', '54'),
(11, 'Armenia', 'AM', '374'),
(12, 'Aruba', 'AW', '297'),
(13, 'Australia', 'AU', '61'),
(14, 'Austria', 'AT', '43'),
(15, 'Azerbaijan', 'AZ', '994'),
(16, 'Bahamas The', 'BS', '1242'),
(17, 'Bahrain', 'BH', '973'),
(18, 'Bangladesh', 'BD', '880'),
(19, 'Barbados', 'BB', '1246'),
(20, 'Belarus', 'BY', '375'),
(21, 'Belgium', 'BE', '32'),
(22, 'Belize', 'BZ', '501'),
(23, 'Benin', 'BJ', '229'),
(24, 'Bermuda', 'BM', '1441'),
(25, 'Bhutan', 'BT', '975'),
(26, 'Bolivia', 'BO', '591'),
(27, 'Bosnia and Herzegovina', 'BA', '387'),
(28, 'Botswana', 'BW', '267'),
(29, 'Bouvet Island', 'BV', '0'),
(30, 'Brazil', 'BR', '55'),
(31, 'British Indian Ocean Territory', 'IO', '246'),
(32, 'Brunei', 'BN', '673'),
(33, 'Bulgaria', 'BG', '359'),
(34, 'Burkina Faso', 'BF', '226'),
(35, 'Burundi', 'BI', '257'),
(36, 'Cambodia', 'KH', '855'),
(37, 'Cameroon', 'CM', '237'),
(38, 'Canada', 'CA', '1'),
(39, 'Cape Verde', 'CV', '238'),
(40, 'Cayman Islands', 'KY', '1345'),
(41, 'Central African Republic', 'CF', '236'),
(42, 'Chad', 'TD', '235'),
(43, 'Chile', 'CL', '56'),
(44, 'China', 'CN', '86'),
(45, 'Christmas Island', 'CX', '61'),
(46, 'Cocos (Keeling) Islands', 'CC', '672'),
(47, 'Colombia', 'CO', '57'),
(48, 'Comoros', 'KM', '269'),
(49, 'Congo', 'CG', '242'),
(50, 'Congo The Democratic Republic Of The', 'CD', '242'),
(51, 'Cook Islands', 'CK', '682'),
(52, 'Costa Rica', 'CR', '506'),
(53, 'Cote D''Ivoire (Ivory Coast)', 'CI', '225'),
(54, 'Croatia (Hrvatska)', 'HR', '385'),
(55, 'Cuba', 'CU', '53'),
(56, 'Cyprus', 'CY', '357'),
(57, 'Czech Republic', 'CZ', '420'),
(58, 'Denmark', 'DK', '45'),
(59, 'Djibouti', 'DJ', '253'),
(60, 'Dominica', 'DM', '1767'),
(61, 'Dominican Republic', 'DO', '1809'),
(62, 'East Timor', 'TP', '670'),
(63, 'Ecuador', 'EC', '593'),
(64, 'Egypt', 'EG', '20'),
(65, 'El Salvador', 'SV', '503'),
(66, 'Equatorial Guinea', 'GQ', '240'),
(67, 'Eritrea', 'ER', '291'),
(68, 'Estonia', 'EE', '372'),
(69, 'Ethiopia', 'ET', '251'),
(70, 'External Territories of Australia', 'XA', '61'),
(71, 'Falkland Islands', 'FK', '500'),
(72, 'Faroe Islands', 'FO', '298'),
(73, 'Fiji Islands', 'FJ', '679'),
(74, 'Finland', 'FI', '358'),
(75, 'France', 'FR', '33'),
(76, 'French Guiana', 'GF', '594'),
(77, 'French Polynesia', 'PF', '689'),
(78, 'French Southern Territories', 'TF', '0'),
(79, 'Gabon', 'GA', '241'),
(80, 'Gambia The', 'GM', '220'),
(81, 'Georgia', 'GE', '995'),
(82, 'Germany', 'DE', '49'),
(83, 'Ghana', 'GH', '233'),
(84, 'Gibraltar', 'GI', '350'),
(85, 'Greece', 'GR', '30'),
(86, 'Greenland', 'GL', '299'),
(87, 'Grenada', 'GD', '1473'),
(88, 'Guadeloupe', 'GP', '590'),
(89, 'Guam', 'GU', '1671'),
(90, 'Guatemala', 'GT', '502'),
(91, 'Guernsey and Alderney', 'XU', '44'),
(92, 'Guinea', 'GN', '224'),
(93, 'Guinea-Bissau', 'GW', '245'),
(94, 'Guyana', 'GY', '592'),
(95, 'Haiti', 'HT', '509'),
(96, 'Heard and McDonald Islands', 'HM', '0'),
(97, 'Honduras', 'HN', '504'),
(98, 'Hong Kong S.A.R.', 'HK', '852'),
(99, 'Hungary', 'HU', '36'),
(100, 'Iceland', 'IS', '354'),
(101, 'India', 'IN', '91'),
(102, 'Indonesia', 'ID', '62'),
(103, 'Iran', 'IR', '98'),
(104, 'Iraq', 'IQ', '964'),
(105, 'Ireland', 'IE', '353'),
(106, 'Israel', 'IL', '972'),
(107, 'Italy', 'IT', '39'),
(108, 'Jamaica', 'JM', '1876'),
(109, 'Japan', 'JP', '81'),
(110, 'Jersey', 'XJ', '44'),
(111, 'Jordan', 'JO', '962'),
(112, 'Kazakhstan', 'KZ', '7'),
(113, 'Kenya', 'KE', '254'),
(114, 'Kiribati', 'KI', '686'),
(115, 'Korea North', 'KP', '850'),
(116, 'Korea South', 'KR', '82'),
(117, 'Kuwait', 'KW', '965'),
(118, 'Kyrgyzstan', 'KG', '996'),
(119, 'Laos', 'LA', '856'),
(120, 'Latvia', 'LV', '371'),
(121, 'Lebanon', 'LB', '961'),
(122, 'Lesotho', 'LS', '266'),
(123, 'Liberia', 'LR', '231'),
(124, 'Libya', 'LY', '218'),
(125, 'Liechtenstein', 'LI', '423'),
(126, 'Lithuania', 'LT', '370'),
(127, 'Luxembourg', 'LU', '352'),
(128, 'Macau S.A.R.', 'MO', '853'),
(129, 'Macedonia', 'MK', '389'),
(130, 'Madagascar', 'MG', '261'),
(131, 'Malawi', 'MW', '265'),
(132, 'Malaysia', 'MY', '60'),
(133, 'Maldives', 'MV', '960'),
(134, 'Mali', 'ML', '223'),
(135, 'Malta', 'MT', '356'),
(136, 'Man (Isle of)', 'XM', '44'),
(137, 'Marshall Islands', 'MH', '692'),
(138, 'Martinique', 'MQ', '596'),
(139, 'Mauritania', 'MR', '222'),
(140, 'Mauritius', 'MU', '230'),
(141, 'Mayotte', 'YT', '269'),
(142, 'Mexico', 'MX', '52'),
(143, 'Micronesia', 'FM', '691'),
(144, 'Moldova', 'MD', '373'),
(145, 'Monaco', 'MC', '377'),
(146, 'Mongolia', 'MN', '976'),
(147, 'Montserrat', 'MS', '1664'),
(148, 'Morocco', 'MA', '212'),
(149, 'Mozambique', 'MZ', '258'),
(150, 'Myanmar', 'MM', '95'),
(151, 'Namibia', 'NA', '264'),
(152, 'Nauru', 'NR', '674'),
(153, 'Nepal', 'NP', '977'),
(154, 'Netherlands Antilles', 'AN', '599'),
(155, 'Netherlands The', 'NL', '31'),
(156, 'New Caledonia', 'NC', '687'),
(157, 'New Zealand', 'NZ', '64'),
(158, 'Nicaragua', 'NI', '505'),
(159, 'Niger', 'NE', '227'),
(160, 'Nigeria', 'NG', '234'),
(161, 'Niue', 'NU', '683'),
(162, 'Norfolk Island', 'NF', '672'),
(163, 'Northern Mariana Islands', 'MP', '1670'),
(164, 'Norway', 'NO', '47'),
(165, 'Oman', 'OM', '968'),
(166, 'Pakistan', 'PK', '92'),
(167, 'Palau', 'PW', '680'),
(168, 'Palestinian Territory Occupied', 'PS', '970'),
(169, 'Panama', 'PA', '507'),
(170, 'Papua new Guinea', 'PG', '675'),
(171, 'Paraguay', 'PY', '595'),
(172, 'Peru', 'PE', '51'),
(173, 'Philippines', 'PH', '63'),
(174, 'Pitcairn Island', 'PN', '0'),
(175, 'Poland', 'PL', '48'),
(176, 'Portugal', 'PT', '351'),
(177, 'Puerto Rico', 'PR', '1787'),
(178, 'Qatar', 'QA', '974'),
(179, 'Reunion', 'RE', '262'),
(180, 'Romania', 'RO', '40'),
(181, 'Russia', 'RU', '70'),
(182, 'Rwanda', 'RW', '250'),
(183, 'Saint Helena', 'SH', '290'),
(184, 'Saint Kitts And Nevis', 'KN', '1869'),
(185, 'Saint Lucia', 'LC', '1758'),
(186, 'Saint Pierre and Miquelon', 'PM', '508'),
(187, 'Saint Vincent And The Grenadines', 'VC', '1784'),
(188, 'Samoa', 'WS', '684'),
(189, 'San Marino', 'SM', '378'),
(190, 'Sao Tome and Principe', 'ST', '239'),
(191, 'Saudi Arabia', 'SA', '966'),
(192, 'Senegal', 'SN', '221'),
(193, 'Serbia', 'RS', '381'),
(194, 'Seychelles', 'SC', '248'),
(195, 'Sierra Leone', 'SL', '232'),
(196, 'Singapore', 'SG', '65'),
(197, 'Slovakia', 'SK', '421'),
(198, 'Slovenia', 'SI', '386'),
(199, 'Smaller Territories of the UK', 'XG', '44'),
(200, 'Solomon Islands', 'SB', '677'),
(201, 'Somalia', 'SO', '252'),
(202, 'South Africa', 'ZA', '27'),
(203, 'South Georgia', 'GS', '0'),
(204, 'South Sudan', 'SS', '211'),
(205, 'Spain', 'ES', '34'),
(206, 'Sri Lanka', 'LK', '94'),
(207, 'Sudan', 'SD', '249'),
(208, 'Suriname', 'SR', '597'),
(209, 'Svalbard And Jan Mayen Islands', 'SJ', '47'),
(210, 'Swaziland', 'SZ', '268'),
(211, 'Sweden', 'SE', '46'),
(212, 'Switzerland', 'CH', '41'),
(213, 'Syria', 'SY', '963'),
(214, 'Taiwan', 'TW', '886'),
(215, 'Tajikistan', 'TJ', '992'),
(216, 'Tanzania', 'TZ', '255'),
(217, 'Thailand', 'TH', '66'),
(218, 'Togo', 'TG', '228'),
(219, 'Tokelau', 'TK', '690'),
(220, 'Tonga', 'TO', '676'),
(221, 'Trinidad And Tobago', 'TT', '1868'),
(222, 'Tunisia', 'TN', '216'),
(223, 'Turkey', 'TR', '90'),
(224, 'Turkmenistan', 'TM', '7370'),
(225, 'Turks And Caicos Islands', 'TC', '1649'),
(226, 'Tuvalu', 'TV', '688'),
(227, 'Uganda', 'UG', '256'),
(228, 'Ukraine', 'UA', '380'),
(229, 'United Arab Emirates', 'AE', '971'),
(230, 'United Kingdom', 'GB', '44'),
(231, 'United States', 'US', '1'),
(232, 'United States Minor Outlying Islands', 'UM', '1'),
(233, 'Uruguay', 'UY', '598'),
(234, 'Uzbekistan', 'UZ', '998'),
(235, 'Vanuatu', 'VU', '678'),
(236, 'Vatican City State (Holy See)', 'VA', '39'),
(237, 'Venezuela', 'VE', '58'),
(238, 'Vietnam', 'VN', '84'),
(239, 'Virgin Islands (British)', 'VG', '1284'),
(240, 'Virgin Islands (US)', 'VI', '1340'),
(241, 'Wallis And Futuna Islands', 'WF', '681'),
(242, 'Western Sahara', 'EH', '212'),
(243, 'Yemen', 'YE', '967'),
(244, 'Yugoslavia', 'YU', '38'),
(245, 'Zambia', 'ZM', '260'),
(246, 'Zimbabwe', 'ZW', '263');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `description` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `id_course_schedule` int(11) NOT NULL,
  `id_course_area` int(11) NOT NULL,
  `id_teacher` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_course_area` (`id_course_area`),
  KEY `id_course_schedule` (`id_course_schedule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course_area`
--

CREATE TABLE IF NOT EXISTS `course_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `description` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `course_area`
--

INSERT INTO `course_area` (`id`, `name`, `description`) VALUES
(1, 'Computación', NULL),
(2, 'Cosmetología', NULL),
(3, 'Corte y confección', NULL),
(4, 'Cocina', NULL),
(5, 'Manualidades', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course_enrollment`
--

CREATE TABLE IF NOT EXISTS `course_enrollment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_student` int(11) NOT NULL,
  `id_course` int(11) NOT NULL,
  `id_project` int(11) DEFAULT NULL,
  `has_experience` tinyint(1) NOT NULL,
  `experience` varchar(250) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_enrollment_reason` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_student` (`id_student`),
  KEY `id_course` (`id_course`),
  KEY `id_enrollment_reason` (`id_enrollment_reason`),
  KEY `id_project` (`id_project`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `course_schedule`
--

CREATE TABLE IF NOT EXISTS `course_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `course_schedule`
--

INSERT INTO `course_schedule` (`id`, `name`) VALUES
(1, 'Matutino'),
(2, 'Vespertino'),
(3, 'Sabatino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `education_level`
--

CREATE TABLE IF NOT EXISTS `education_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `education_level`
--

INSERT INTO `education_level` (`id`, `name`) VALUES
(1, 'Sin estudio'),
(2, '1 - 3 grado'),
(3, '4 - 6 grado'),
(4, '7 - 9 grado'),
(5, 'Bachillerato'),
(6, 'Técnico'),
(7, 'Universidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emergency_contact`
--

CREATE TABLE IF NOT EXISTS `emergency_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `phone_number` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `relationship` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `emergency_contact`
--

INSERT INTO `emergency_contact` (`id`, `first_name`, `last_name`, `phone_number`, `relationship`) VALUES
(1, 'Tech', 'Admin', '77420472', 'sysadmin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enrollment_reason`
--

CREATE TABLE IF NOT EXISTS `enrollment_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `identification_type`
--

CREATE TABLE IF NOT EXISTS `identification_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `identification_type`
--

INSERT INTO `identification_type` (`id`, `name`) VALUES
(1, 'DUI'),
(2, 'NIT'),
(3, 'Partida de nacimiento'),
(4, 'Pasaporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media_type`
--

CREATE TABLE IF NOT EXISTS `media_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `media_type`
--

INSERT INTO `media_type` (`id`, `name`) VALUES
(1, 'Internet'),
(2, 'Facebook'),
(3, 'Televisión'),
(4, 'Revista'),
(5, 'Periódico'),
(6, 'Afiches'),
(7, 'Amigo'),
(8, 'Otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medical_record`
--

CREATE TABLE IF NOT EXISTS `medical_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sickness` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `medicament` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `disability` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `id_sponsor` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_sponsor` (`id_sponsor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `religion`
--

CREATE TABLE IF NOT EXISTS `religion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `religion`
--

INSERT INTO `religion` (`id`, `name`) VALUES
(1, 'Católica'),
(2, 'Cristiana no católica'),
(3, 'No cristiana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sponsor`
--

CREATE TABLE IF NOT EXISTS `sponsor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `contact_name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_spanish2_ci NOT NULL,
  `phone_number` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `last_update` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(75) COLLATE utf8_spanish2_ci NOT NULL,
  `pass` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `identification` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `id_identification_type` int(11) NOT NULL,
  `id_emergency_contact` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `id_identification_type` (`id_identification_type`),
  KEY `id_emergency_contact` (`id_emergency_contact`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `staff`
--

INSERT INTO `staff` (`id`, `first_name`, `last_name`, `email`, `pass`, `phone_number`, `identification`, `id_identification_type`, `id_emergency_contact`) VALUES (1, 'Tech', 'Admin', 'tanpalma04@gmail.com', md5('1234'), '77420472', '05143948-0', '1', '1');
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `identification_number` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `landline_phone` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cellphone` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `number_of_children` int(11) DEFAULT NULL,
  `profession` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `literate` tinyint(1) NOT NULL,
  `head_of_household` tinyint(1) NOT NULL,
  `id_civil_status` int(11) NOT NULL,
  `id_education_level` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_town` int(11) NOT NULL,
  `id_identification_type` int(11) NOT NULL,
  `id_religion` int(11) NOT NULL,
  `id_emergency_contact` int(11) NOT NULL,
  `id_medical_record` int(11) NOT NULL,
  `id_media_type` int(11) NOT NULL COMMENT 'How did you hear about us?',
  PRIMARY KEY (`id`),
  KEY `id_country` (`id_country`),
  KEY `id_identification_type` (`id_identification_type`),
  KEY `id_civil_status` (`id_civil_status`),
  KEY `id_education_level` (`id_education_level`),
  KEY `id_town` (`id_town`),
  KEY `id_religion` (`id_religion`),
  KEY `id_emergency_contact` (`id_emergency_contact`),
  KEY `id_medical_record` (`id_medical_record`),
  KEY `id_media_type` (`id_media_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `town`
--

CREATE TABLE IF NOT EXISTS `town` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `id_city` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_city` (`id_city`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=263 ;

--
-- Volcado de datos para la tabla `town`
--

INSERT INTO `town` (`id`, `name`, `id_city`) VALUES
(1, 'Ahuachapán', 1),
(2, 'Jujutla', 1),
(3, 'Atiquizaya', 1),
(4, 'Concepción de Ataco', 1),
(5, 'El Refugio', 1),
(6, 'Guaymango', 1),
(7, 'Apaneca', 1),
(8, 'San Francisco Menéndez', 1),
(9, 'San Lorenzo', 1),
(10, 'San Pedro Puxtla', 1),
(11, 'Tacuba', 1),
(12, 'Turín', 1),
(13, 'Cinquera', 2),
(14, 'Villa Dolores', 2),
(15, 'Guacotecti', 2),
(16, 'Ilobasco', 2),
(17, 'Jutiapa', 2),
(18, 'San Isidro', 2),
(19, 'Sensuntepeque', 2),
(20, 'Ciudad de Tejutepeque', 2),
(21, 'Victoria', 2),
(22, 'Agua Caliente', 3),
(23, 'Arcatao', 3),
(24, 'Azacualpa', 3),
(25, 'Chalatenango', 3),
(26, 'Citalá', 3),
(27, 'Comalapa', 3),
(28, 'Concepción Quezaltepeque', 3),
(29, 'Dulce Nombre de María', 3),
(30, 'El Carrizal', 3),
(31, 'El Paraíso', 3),
(32, 'La Laguna', 3),
(33, 'La Palma', 3),
(34, 'La Reina', 3),
(35, 'Las Vueltas', 3),
(36, 'Nombre de Jesús', 3),
(37, 'Nueva Concepción', 3),
(38, 'Nueva Trinidad', 3),
(39, 'Ojos de Agua', 3),
(40, 'Potonico', 3),
(41, 'San Antonio de la Cruz', 3),
(42, 'San Antonio Los Ranchos', 3),
(43, 'San Fernando', 3),
(44, 'San Francisco Lempa', 3),
(45, 'San Francisco Morazán', 3),
(46, 'San Ignacio', 3),
(47, 'San Isidro Labrador', 3),
(48, 'San José Cancasque', 3),
(49, 'San José Las Flores', 3),
(50, 'San Luis del Carmen', 3),
(51, 'San Miguel de Mercedes', 3),
(52, 'San Rafael', 3),
(53, 'Santa Rita', 3),
(54, 'Tejutla', 3),
(55, 'Candelaria', 4),
(56, 'Cojutepeque', 4),
(57, 'El Carmen', 4),
(58, 'El Rosario', 4),
(59, 'Monte San Juan', 4),
(60, 'Oratorio de Concepción', 4),
(61, 'San Bartolomé Perulapía', 4),
(62, 'San Cristóbal', 4),
(63, 'San José Guayabal', 4),
(64, 'San Pedro Perulapán', 4),
(65, 'San Rafael Cedros', 4),
(66, 'San Ramón', 4),
(67, 'Santa Cruz Analquito', 4),
(68, 'Santa Cruz Michapa', 4),
(69, 'Suchitoto', 4),
(70, 'Tenancingo', 4),
(71, 'Antiguo Cuscatlán', 5),
(72, 'Chiltiupán', 5),
(73, 'Ciudad Arce', 5),
(74, 'Colón', 5),
(75, 'Comasagua', 5),
(76, 'Huizúcar', 5),
(77, 'Jayaque', 5),
(78, 'Jicalapa', 5),
(79, 'La Libertad', 5),
(80, 'Nueva San Salvador', 5),
(81, 'Nuevo Cuscatlán', 5),
(82, 'Opico', 5),
(83, 'Quezaltepeque', 5),
(84, 'Sacacoyo', 5),
(85, 'San José Villanueva', 5),
(86, 'San Matías', 5),
(87, 'San Pablo Tacachico', 5),
(88, 'Talnique', 5),
(89, 'Tamanique', 5),
(90, 'Teotepeque', 5),
(91, 'Tepecoyo', 5),
(92, 'Zaragoza', 5),
(93, 'Cuyultitán', 6),
(94, 'El Rosario', 6),
(95, 'Jerusalén', 6),
(96, 'Mercedes La Ceiba', 6),
(97, 'Olocuilta', 6),
(98, 'Paraíso de Osorio', 6),
(99, 'San Antonio Masahuat', 6),
(100, 'San Emigdio', 6),
(101, 'San Francisco Chinameca', 6),
(102, 'San Juan Nonualco', 6),
(103, 'San Juan Talpa', 6),
(104, 'San Juan Tepezontes', 6),
(105, 'San Luis La Herradura', 6),
(106, 'San Luis Talpa', 6),
(107, 'San Miguel Tepezontes', 6),
(108, 'San Pedro Masahuat', 6),
(109, 'San Pedro Nonualco', 6),
(110, 'San Rafael Obrajuelo', 6),
(111, 'Santa María Ostuma', 6),
(112, 'Santiago Nonualco', 6),
(113, 'Tapalhuaca', 6),
(114, 'Zacatecoluca', 6),
(115, 'Anamorós', 7),
(116, 'Bolívar', 7),
(117, 'Concepción de Oriente', 7),
(118, 'Conchagua', 7),
(119, 'El Carmen', 7),
(120, 'El Sauce', 7),
(121, 'Intipucá', 7),
(122, 'La Unión', 7),
(123, 'Lislique', 7),
(124, 'Meanguera del Golfo', 7),
(125, 'Nueva Esparta', 7),
(126, 'Pasaquina', 7),
(127, 'Polorós', 7),
(128, 'San Alejo', 7),
(129, 'San José', 7),
(130, 'Santa Rosa de Lima', 7),
(131, 'Yayantique', 7),
(132, 'Yucuayquín', 7),
(133, 'Arambala', 8),
(134, 'Cacaopera', 8),
(135, 'Chilanga', 8),
(136, 'Corinto', 8),
(137, 'Delicias de Concepción', 8),
(138, 'El Divisadero', 8),
(139, 'El Rosario', 8),
(140, 'Gualococti', 8),
(141, 'Guatajiagua', 8),
(142, 'Joateca', 8),
(143, 'Jocoaitique', 8),
(144, 'Jocoro', 8),
(145, 'Lolotiquillo', 8),
(146, 'Meanguera', 8),
(147, 'Osicala', 8),
(148, 'Perquín', 8),
(149, 'San Carlos', 8),
(150, 'San Fernando', 8),
(151, 'San Francisco Gotera', 8),
(152, 'San Isidro', 8),
(153, 'San Simón', 8),
(154, 'Sensembra', 8),
(155, 'Sociedad', 8),
(156, 'Torola', 8),
(157, 'Yamabal', 8),
(158, 'Yoloaiquín', 8),
(159, 'Carolina', 9),
(160, 'Chapeltique', 9),
(161, 'Chinameca', 9),
(162, 'Chirilagua', 9),
(163, 'Ciudad Barrios', 9),
(164, 'Comacarán', 9),
(165, 'El Tránsito', 9),
(166, 'Lolotique', 9),
(167, 'Moncagua', 9),
(168, 'Nueva Guadalupe', 9),
(169, 'Nuevo Edén de San Juan', 9),
(170, 'Quelepa', 9),
(171, 'San Antonio', 9),
(172, 'San Gerardo', 9),
(173, 'San Jorge', 9),
(174, 'San Luis de la Reina', 9),
(175, 'San Miguel', 9),
(176, 'San Rafael', 9),
(177, 'Sesori', 9),
(178, 'Uluazapa', 9),
(179, 'Aguilares', 10),
(180, 'Apopa', 10),
(181, 'Ayutuxtepeque', 10),
(182, 'Cuscatancingo', 10),
(183, 'Delgado', 10),
(184, 'El Paisnal', 10),
(185, 'Guazapa', 10),
(186, 'Ilopango', 10),
(187, 'Mejicanos', 10),
(188, 'Nejapa', 10),
(189, 'Panchimalco', 10),
(190, 'Rosario de Mora', 10),
(191, 'San Marcos', 10),
(192, 'San Martín', 10),
(193, 'San Salvador', 10),
(194, 'Santiago Texacuangos', 10),
(195, 'Santo Tomás', 10),
(196, 'Soyapango', 10),
(197, 'Tonacatepeque', 10),
(198, 'Apastepeque', 11),
(199, 'Guadalupe', 11),
(200, 'San Cayetano Istepeque', 11),
(201, 'San Esteban Catarina', 11),
(202, 'San Ildefonso', 11),
(203, 'San Lorenzo', 11),
(204, 'San Sebastián', 11),
(205, 'Santa Clara', 11),
(206, 'Santo Domingo', 11),
(207, 'San Vicente', 11),
(208, 'Tecoluca', 11),
(209, 'Tepetitán', 11),
(210, 'Verapaz', 11),
(211, 'Candelaria de la Frontera', 12),
(212, 'Chalchuapa', 12),
(213, 'Coatepeque', 12),
(214, 'El Congo', 12),
(215, 'El Porvenir', 12),
(216, 'Masahuat', 12),
(217, 'Metapán', 12),
(218, 'San Antonio Pajonal', 12),
(219, 'San Sebastián Salitrillo', 12),
(220, 'Santa Ana', 12),
(221, 'Santa Rosa Guachipilín', 12),
(222, 'Santiago de la Frontera', 12),
(223, 'Texistepeque', 12),
(224, 'Acajutla', 13),
(225, 'Armenia', 13),
(226, 'Caluco', 13),
(227, 'Cuisnahuat', 13),
(228, 'Izalco', 13),
(229, 'Juayúa', 13),
(230, 'Nahuizalco', 13),
(231, 'Nahulingo', 13),
(232, 'Salcoatitán', 13),
(233, 'San Antonio del Monte', 13),
(234, 'San Julián', 13),
(235, 'Santa Catarina Masahuat', 13),
(236, 'Santa Isabel Ishuatán', 13),
(237, 'Santo Domingo', 13),
(238, 'Sonsonate', 13),
(239, 'Sonzacate', 13),
(240, 'Alegría', 14),
(241, 'Berlín', 14),
(242, 'California', 14),
(243, 'Concepción Batres', 14),
(244, 'El Triunfo', 14),
(245, 'Ereguayquín', 14),
(246, 'Estanzuelas', 14),
(247, 'Jiquilisco', 14),
(248, 'Jucuapa', 14),
(249, 'Jucuarán', 14),
(250, 'Mercedes Umaña', 14),
(251, 'Nueva Granada', 14),
(252, 'Ozatlán', 14),
(253, 'Puerto El Triunfo', 14),
(254, 'San Agustín', 14),
(255, 'San Buenaventura', 14),
(256, 'San Dionisio', 14),
(257, 'San Francisco Javier', 14),
(258, 'Santa Elena', 14),
(259, 'Santa María', 14),
(260, 'Santiago de María', 14),
(261, 'Tecapán', 14),
(262, 'Usulután', 14);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`id_course_area`) REFERENCES `course_area` (`id`),
  ADD CONSTRAINT `course_ibfk_2` FOREIGN KEY (`id_course_schedule`) REFERENCES `course_schedule` (`id`);

--
-- Filtros para la tabla `course_enrollment`
--
ALTER TABLE `course_enrollment`
  ADD CONSTRAINT `course_enrollment_ibfk_4` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `course_enrollment_ibfk_1` FOREIGN KEY (`id_student`) REFERENCES `student` (`id`),
  ADD CONSTRAINT `course_enrollment_ibfk_2` FOREIGN KEY (`id_course`) REFERENCES `course` (`id`),
  ADD CONSTRAINT `course_enrollment_ibfk_3` FOREIGN KEY (`id_enrollment_reason`) REFERENCES `enrollment_reason` (`id`);

--
-- Filtros para la tabla `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`id_sponsor`) REFERENCES `sponsor` (`id`);

--
-- Filtros para la tabla `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_ibfk_2` FOREIGN KEY (`id_identification_type`) REFERENCES `identification_type` (`id`),
  ADD CONSTRAINT `staff_ibfk_3` FOREIGN KEY (`id_emergency_contact`) REFERENCES `emergency_contact` (`id`);

--
-- Filtros para la tabla `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_9` FOREIGN KEY (`id_media_type`) REFERENCES `media_type` (`id`),
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`id_country`) REFERENCES `country` (`id`),
  ADD CONSTRAINT `student_ibfk_2` FOREIGN KEY (`id_identification_type`) REFERENCES `identification_type` (`id`),
  ADD CONSTRAINT `student_ibfk_3` FOREIGN KEY (`id_civil_status`) REFERENCES `civil_status` (`id`),
  ADD CONSTRAINT `student_ibfk_4` FOREIGN KEY (`id_education_level`) REFERENCES `education_level` (`id`),
  ADD CONSTRAINT `student_ibfk_5` FOREIGN KEY (`id_town`) REFERENCES `town` (`id`),
  ADD CONSTRAINT `student_ibfk_6` FOREIGN KEY (`id_religion`) REFERENCES `religion` (`id`),
  ADD CONSTRAINT `student_ibfk_7` FOREIGN KEY (`id_emergency_contact`) REFERENCES `emergency_contact` (`id`),
  ADD CONSTRAINT `student_ibfk_8` FOREIGN KEY (`id_medical_record`) REFERENCES `medical_record` (`id`);

--
-- Filtros para la tabla `town`
--
ALTER TABLE `town`
  ADD CONSTRAINT `town_ibfk_1` FOREIGN KEY (`id_city`) REFERENCES `city` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
